/*
 * Kiwi Favicon Package
 * Description: This package will get and cache webiste favicons
 * Version: 1.0
 * Authors: Earl Balai Jr @earlbalai & Mikkel Hansen @mikfoo
 * Copyright(c) 2015 kiwi.io All Rights Reserved.
 */
package favicon

import (
  "fmt"
  "io"
  "net/http"
  "net/url"
  "os"
  "path/filepath"
  "strings"

  "golang.org/x/net/html"
)

type FaviconBlock struct {
  Host     string
  Path     string
  Favicons []*Favicon
}

type Favicon struct {
  Name string
  Url  string
  File string // Empty if not downloaded
}

func (f *Favicon) Download() {

  u, err := url.Parse(f.Url)
  if err != nil {
    panic(err)
  }

  dir, dir2 := "cache", u.Host
  mainDir := filepath.Join(dir, dir2)
  os.MkdirAll(mainDir, 0755)

  output, err := os.Create(filepath.Join(mainDir, f.Name))
  if err != nil {
    panic(err)
  }
  defer output.Close()

  r, err := http.Get(f.Url)
  if err != nil {
    panic(err)
  }
  defer r.Body.Close()

  n, err := io.Copy(output, r.Body)
  if err != nil {
    panic(err)
  }
  f.File = output.Name()
  fmt.Println("Cached", f.Name, "-", n, "bytes")
}

func (fb *FaviconBlock) DownloadAll() {
  for _, f := range fb.Favicons {
    f.Download()
  }
}

func (f *Favicon) IsDownloaded() bool {
  return f.File != ""
}

func checkDefault(url string) (success bool, err error) {
  r, err := http.Get(url + "/favicon.ico")
  if err != nil {
    return false, err
  }
  if r.StatusCode != 200 {
    return false, fmt.Errorf("Expected status code 200, got %d", r.StatusCode)
  } else if r.StatusCode == 200 {
    return true, nil
  }
  return
}

func Get(uri string) *FaviconBlock {
  url, err := url.Parse(uri)
  if err != nil {
    panic(err)
  }

  icons := &FaviconBlock{
    url.Host,
    url.RequestURI(),
    nil,
  }

  fmt.Println("Getting icons for", uri)

  r, err := http.Get(uri)
  if err != nil {
    panic(err)
  }
  defer r.Body.Close()

  page := html.NewTokenizer(r.Body)
  for {
    tokenType := page.Next()

    if tokenType == html.ErrorToken {
      defaultFavicon, err := checkDefault(uri)
      if err != nil {
        fmt.Println("Error:", err)
      }
      if defaultFavicon {
        icons.Favicons = append(icons.Favicons, &Favicon{
          "favicon.ico",        // Icon name
          uri + "/favicon.ico", // Url
          "",                   // File - empty by default
        })
      }
      return icons
    }
    token := page.Token()
    href, found := "", false
    if tokenType == html.StartTagToken && token.DataAtom.String() == "link" {
      for _, elem := range token.Attr {
        if elem.Key == "href" {
          href = elem.Val
        } else if elem.Key == "rel" {
          possible := []string{"icon", "shortcut icon"}
          for _, k := range possible {
            if elem.Val == k {
              found = true
            }
          }
        }
      }
      if found && href != "" {
        fUrl := url.String() + "/" + href
        tokens := strings.Split(fUrl, "/")
        fileName := tokens[len(tokens)-1]
        //fmt.Println("Added", fileName)
        icons.Favicons = append(icons.Favicons, &Favicon{
          fileName, // Icon name
          fUrl,     // Url
          "",       // File - empty by default
        })
      }
    }
  }
}
